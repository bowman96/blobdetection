import cv2
import numpy as np

#loads in image
img = cv2.imread('images/webstershapes.png')

#processes image
blurred = cv2.pyrMeanShiftFiltering(img, 31, 31)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
denoised1 = cv2.fastNlMeansDenoising(gray, None, 1, 7, 21)
denoised2 = gray - denoised1

thresholdValue = 100;
maxValue = 101;
ret, binaryThreshold = cv2.threshold(gray, thresholdValue, maxValue, cv2.THRESH_BINARY)
ret, binaryThresholdOtsu = cv2.threshold(gray, thresholdValue, maxValue, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
ret, invBinaryThreshold = cv2.threshold(gray, thresholdValue, maxValue, cv2.THRESH_BINARY_INV)
ret, invBinaryThresholdOtsu = cv2.threshold(gray, thresholdValue, maxValue, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

#displaying image
cv2.namedWindow('OriginalImage', cv2.WINDOW_NORMAL)
cv2.namedWindow('DenoisedImage1', cv2.WINDOW_NORMAL)
cv2.namedWindow('DenoisedImage2', cv2.WINDOW_NORMAL)
cv2.namedWindow('GrayImage', cv2.WINDOW_NORMAL)
""" cv2.namedWindow('BinaryThreshold', cv2.WINDOW_NORMAL)
cv2.namedWindow('BinaryThresholdOtsu', cv2.WINDOW_NORMAL)
cv2.namedWindow('InvBinaryThreshold', cv2.WINDOW_NORMAL)
cv2.namedWindow('InvBinaryThresholdOtsu', cv2.WINDOW_NORMAL) """
cv2.namedWindow('DisplayBlurred', cv2.WINDOW_NORMAL)
cv2.imshow('OriginalImage', img)
cv2.imshow('DenoisedImage1', denoised1)
cv2.imshow('DenoisedImage2', denoised2)
cv2.imshow('GrayImage', gray)
""" cv2.imshow('BinaryThreshold', binaryThreshold)
cv2.imshow('BinaryThresholdOtsu', binaryThresholdOtsu)
cv2.imshow('InvBinaryThreshold', invBinaryThreshold)
cv2.imshow('InvBinaryThresholdOtsu', invBinaryThresholdOtsu) """
cv2.imshow('DisplayBlurred', blurred)
cv2.waitKey(0)