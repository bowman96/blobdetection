import cv2
import numpy as np

#loads in image
img = cv2.imread('images/webstershapes.png')

#processes image
blurred = cv2.pyrMeanShiftFiltering(img, 31, 31)
gray = cv2.cvtColor(blurred, cv2.COLOR_BGR2GRAY)
ret, threshold = cv2.threshold(gray, 100, 101, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

#contour detection
th, contours, hierarchy = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
print('Number of Contours Detected: %d' %len(contours)) 

#draws contours onto original image
for contour in contours:
  rect = cv2.minAreaRect(contour)
  box = cv2.boxPoints(rect)
  box = np.int0(box)
  area = cv2.contourArea(contour)
  
  if area > 0:
    cv2.drawContours(img, [box], -1, (0, 255, 0), 1)


#displaying image
cv2.namedWindow('DisplayImage', cv2.WINDOW_NORMAL)
cv2.namedWindow('DisplayBlurred', cv2.WINDOW_NORMAL)
cv2.namedWindow('DisplayThreshold', cv2.WINDOW_NORMAL)
cv2.imshow('DisplayImage', img)
cv2.imshow('DisplayBlurred', blurred)
cv2.imshow('DisplayThreshold', threshold)
cv2.waitKey(0)